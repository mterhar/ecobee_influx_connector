# Ecobee -> InfluxDB Connector

This repo is a fork from [cdzombak/ecobee_influx_connector](https://github.com/cdzombak/ecobee_influx_connector).

Changes:

* Timeout or keypress to continue during startup where credentials are needed
* Pulls all thermostats rather than just one (removed thermostat_id)

Changes done in an amateurish way for now.
Planning to refactor the main loop to make it clearer what's happening and use the ecobee api to select all the thermostats rather than running the update command multiple times per update.

## Getting Started

1. Register and enable the developer dashboard on your Ecobee account at https://www.ecobee.com/developers/
2. Go to https://www.ecobee.com/consumerportal/index.html , navigate to Developer in the right-hand menu, and create an App.
3. Create a JSON config file containing that API key and a working directory.
4. Go to https://www.ecobee.com/consumerportal/index.html , navigate to My Apps in the right-hand menu, and click Add Application.
5. Run the application to generate a pin. If it's running in a container, you have 60 seconds to enter the pin before it tries again. Paste the PIN there and authorize the app.
6. Return to the `ecobee_influx_connector` CLI and hit Enter or wait for the timeout.

### Docker compose

If you want to self-host the influxdb and run the ecobee influx connector, you can use the `docker-compose.yml` file to do so.

Steps to get it working include some clickOps thanks to InfluxDB disregarding environment variables or something.
I'm probably doing that part wrong, but it's bootstrapped now so who cares.

```shell
docker compose up influxdb
```

1. Once that starts, open it in a browser.
2. Create a bucket
3. Create an API key for that bucket
4. Put the API key in the `config.json` file based on the `config.example.json` file.
5. `docker compose up ecobee-influx`
6. When you see the pin, go to the [Ecobee web app > My Apps](https://www.ecobee.com/consumerportal/index.html) and approve the pin.
7. Enjoy!

## Configure

Configuration is specified in a JSON file. Create a file (based on the template `config.example.json` stored in this repository) and customize it with your Ecobee API key, Influx server, and write preferences.

Use the `write_*` config fields to tell the connector which pieces of equipment you use.

The `work_dir` is where client credentials and (yet to be implemented) last-written watermarks are stored.

## Run via Docker

A Docker image is also provided that can be configured via environment variables. [View it on Docker Hub](https://hub.docker.com/r/cdzombak/ecobee_influx_connector), or pull it via `docker pull cdzombak/ecobee_influx_connector`.

To use the docker container make sure the path to the `config.json` is provided as a volume with the path `/config`. This location will also be used to store the refresh token.

Example usage: `docker run -v $HOME/.ecobee_influx_connector:/config -it ecobee_influx_connector`

## Build

```shell
go build -o ./ecobee_influx_connector .
```

To cross-compile for eg. Linux/amd64:

```shell
env GOOS=linux GOARCH=amd64 go build -o ./ecobee_influx_connector .
```

## Install & Run via systemd on Linux

1. Build the `ecobee_influx_connector` binary per the Build instructions above.
2. Copy it to `/usr/local/bin` or your preferred location.
3. Create a work directory for the connector. (I put this at `$HOME/.ecobee_influx_connector`.)
4. Run `chmod 700 $YOUR_NEW_WORK_DIR`. (For my work directory, I ran `chmod 700 $HOME/.ecobee_influx_connector`.)
5. Create a configuration JSON file, per the Configure instructions above. (I put this at `$HOME/.ecobee_influx_connector/config.json`.)
6. Customize [`ecobee-influx-connector.service`](https://raw.githubusercontent.com/cdzombak/ecobee_influx_connector/main/ecobee-influx-connector.service) with your user/group name and the path to your config file.
7. Copy that customized `ecobee-influx-connector.service` to `/etc/systemd/system`.
8. Run `chown root:root /etc/systemd/system/ecobee-influx-connector.service`.
9. Run `systemctl daemon-reload && systemctl enable ecobee-influx-connector.service && systemctl start ecobee-influx-connector.service`.
10. Check the service's status with `systemctl status ecobee-influx-connector.service`.

## FAQ

### Does the connector support multiple thermostats?

The connector does not directly support multiple thermostats. To support this use case, I'd recommend running multiple copies of the connector. Each copy will need its own working directory and config file, but you should be able to use the same API key for each connector instance.

(If deploying using the "systemd on Linux" instructions, give each connector's service file a unique name, like `ecobee-influx-connector-1.service`, `ecobee-influx-connector-2.service`, and so on.

## License

Apache 2; see `LICENSE` in this repository.

## Author

[Chris Dzombak](https://www.dzombak.com) (GitHub [@cdzombak](https://github.com/cdzombak)).
